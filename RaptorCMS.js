Requests = new Mongo.Collection("requests");

//Routing
Router.route('/', function () {
    this.render('list');
});

Router.route('/request/register', function () {
    this.render('register');
});

Router.route('/request/:_id', function () {
    var request = Requests.findOne({_id: this.params._id});
    this.render('request', {data: request});
});
