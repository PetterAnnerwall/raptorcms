Meteor.methods({
  registerRequest: function(title, description){
    Requests.insert({
      title:title,
      description:description,
      created: new Date(),
      updated: new Date(),
      status: 'open'
    });
  },

  updateRequest: function(RequestId, title, description, status){
      console.log(RequestId +' '+ title +' '+ description +' '+ status);
      Requests.update({_id:RequestId}, {$set:{
          title: title,
          description: description,
          status: status,
          updated: new Date()
      }});

  },

  deleteRequest: function(RequestId){
    Requests.remove(RequestId);
  }
});
