if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
    Meteor.publish("requests", function(){
      return Requests.find({},{sort:{created: -1}});
    });

  });
}
