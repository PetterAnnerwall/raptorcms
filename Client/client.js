var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

if (Meteor.isClient) {

  Meteor.subscribe("requests");

  Template.register.events({
    "submit .new-request": function(event){
      event.preventDefault();

      var title = event.target.title.value;
      var description = event.target.description.value;

      Meteor.call("registerRequest", title, description);

      event.target.title.value = "";
      event.target.description.value = "";

    }
  });

  Template.list.events({
    "click .remove": function(event){
      event.preventDefault();
      Meteor.call("deleteRequest", this._id);
    }
  });

  Template.list.helpers({
    requests: function(){
      return Requests.find({});
   }
  });

  Template.list.rendered = function(){
    this.find('div .panel-group')._uihooks = {
      insertElement: function(node, next) {
        $(node).insertBefore(next).addClass("animated zoomIn").one(animationEnd, function(){
          $(node).removeClass("fadeIn");
        });
      },
      removeElement: function(node) {
        $(node).addClass("zoomOut").one(animationEnd, function(){
          $(node).slideUp('fast', function(){
            $(node).remove();
          })
        });
      }
    };
  }

  Template.request.events({
      "submit .edit-request": function(event, template){
          var self = this;
          var title = event.target.title.value;
          var description = event.target.description.value;
          var status = event.target.status.value;
          //var status = event.target.status.value;
          Meteor.call("updateRequest", self._id, title, description, status, function(error, result){
              if(error){
                  console.log("error", error);
              }
              if(result){

              }
          });
      }
  });

  Template.request.helpers({
      statusSelected: function(option){
          if (option == this.status ) {
              return 'selected';
          }
      }
  });

  //helpers

  Template.registerHelper("sinceDate", function(timestamp) {
      return moment(new Date(timestamp)).fromNow();
  });
  Template.registerHelper("prettyTitle", function(title) {
      if (title.length > 40){
        return title.substring(0,40) + '...';}
      return title;
  });

}
